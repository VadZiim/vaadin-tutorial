package com.jpsolution.vaadin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@SuppressWarnings("serial")
@Entity
@Table(name = "CATEGORY")
public class Category extends AbstractEntity  implements Serializable, Cloneable{

	private String category;

	public Category() {
		this.category = "Category";
	}

	@Column(name = "NAME")
	@NotNull(message = "Hotel category is required")
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return category;
	}

	@Override
	public Category clone() throws CloneNotSupportedException {
		return (Category) super.clone();
	}

	@Override
	public int hashCode() {
		int result = 1;
		result = 31 * result + (category != null ? category.hashCode() : 0);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Category))
			return false;
		Category other = (Category) obj;

		return id != null ? id.equals(other.id) : other.id == null;
	}

}

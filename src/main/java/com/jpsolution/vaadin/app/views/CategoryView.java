package com.jpsolution.vaadin.app.views;


import com.jpsolution.vaadin.app.form.CategoryForm;
import com.jpsolution.vaadin.entity.Category;
import com.jpsolution.vaadin.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

@SuppressWarnings({ "serial" })
@SpringUI
public class CategoryView extends AbstractEntityView {
	// set name of the page
	public static String VIEW_NAME = "Categories";

	private ListSelect<Category> list = new ListSelect<>();
	private CategoryForm form;

	/**
	 * Method that is called each time on view enter
	 */
	@Override
	public void enter(ViewChangeEvent event) {
		form = new CategoryForm(this);
		// add buttons
		HorizontalLayout main = new HorizontalLayout(list, form);
		addComponents(getButtons(), main);		;
		updateList();

		list.setItemCaptionGenerator(Category::getCategory);
		list.setWidth(16, Unit.EM);

		form.setVisible(false);

		list.addSelectionListener(selEvent -> {
			form.closeForm();
			if (selEvent == null) {
				enableButtons(false, false);
				return;
			}
			// manage buttons
			int selected = selEvent.getAllSelectedItems().size();
			// edit available only for 1 selected element
			// delete -- for several
			enableButtons(selected == 1, selected > 0);
		});
	}

	@Override
	protected void delete() {
		list.getSelectedItems().stream().forEach(i -> EntityService.getCategoryService().deleteCategory(i));
		Notification not = new Notification("Category sucessfully deleted!");
		not.show(UI.getCurrent().getPage());
		updateList();
	}

	@Override
	protected void edit() {
		form.setVisible(true);
		form.setItem(list.getSelectedItems().iterator().next());
	}

	@Override
	protected void add() {
		Category cat = new Category();
		form.setVisible(true);
		form.setItem(cat);
	}

	public void updateList() {
		list.setItems(EntityService.getCategoryService().findAll());
	}
}
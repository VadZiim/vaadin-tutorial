package com.jpsolution.vaadin.app.views;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jpsolution.vaadin.app.form.HotelForm;
import com.jpsolution.vaadin.app.form.PopupForm;
import com.jpsolution.vaadin.entity.Hotel;
import com.jpsolution.vaadin.service.EntityService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.*;
import org.vaadin.viritin.util.HtmlElementPropertySetter;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.Grid.SelectionMode;

@SuppressWarnings("serial")
@SpringUI
public class HotelView extends AbstractEntityView {
	// set name of the page
	public static String VIEW_NAME = "Hotels";
	// set button bulk update
	private static String BULK_UPDATE = "Bulk update";
	private Button updateButton = new Button(BULK_UPDATE);
	private HotelForm form;
	private Grid<Hotel> grid = new Grid<>(Hotel.class);
	// add filter
	private TextField filterName = new TextField();
	private TextField filterAddress = new TextField();
	// add popup
	private PopupForm popupContent = new PopupForm(this);//b
 	private PopupView popup = new PopupView(null, popupContent);//b
	/**
	 * Method that is called each time on view enter
	 */
	@Override
	public void enter(ViewChangeEvent event) {
		form = new HotelForm(this);
		HorizontalLayout controlPannel = new HorizontalLayout();
		initControls(controlPannel);
		initGridComponent();
		// hide form by default
		form.setVisible(false);
		HorizontalLayout main = new HorizontalLayout(grid, form);
		main.setSizeFull();
		main.setExpandRatio(grid, 1);
		addComponents(controlPannel, main);
	}

	// controls
	private void initControls(HorizontalLayout controlPannel) {
		//filter by name
		filterName.setPlaceholder("filtering by name...");
        filterName.addValueChangeListener(e -> updateListByName());
        filterName.setValueChangeMode(ValueChangeMode.LAZY);
		filterName.setStyleName(ValoTheme.TEXTFIELD_TINY);
		//clean button
		Button clearFilterNameButton = new Button(VaadinIcons.REFRESH);
		clearFilterNameButton.setDescription("Name filter...");
		clearFilterNameButton.addClickListener(e -> filterName.clear());
		clearFilterNameButton.setStyleName(ValoTheme.BUTTON_TINY);

		//filter by name
        filterAddress.setPlaceholder("filtering by address...");
        filterAddress.addValueChangeListener(e -> updateListByAddress());
        filterAddress.setValueChangeMode(ValueChangeMode.LAZY);
		filterAddress.setStyleName(ValoTheme.TEXTFIELD_TINY);
		//clean button
        Button clearFilterAdressButton = new Button(VaadinIcons.REFRESH);
        clearFilterAdressButton.setDescription("Address filter...");
        clearFilterAdressButton.addClickListener(e -> filterAddress.clear());
		clearFilterAdressButton.setStyleName(ValoTheme.BUTTON_TINY);

		// make the field look like HTML5 search
		HtmlElementPropertySetter s1 = new HtmlElementPropertySetter(filterName);
		s1.setProperty("type", "search");
		HtmlElementPropertySetter s2 = new HtmlElementPropertySetter(filterAddress);
		s2.setProperty("type", "search");
		HorizontalLayout buttons = getButtons();

		// add updateButton
		buttons.addComponent(updateButton);
		controlPannel.addComponents(filterName, clearFilterNameButton, filterAddress, clearFilterAdressButton, buttons);
		updateButton.addClickListener(l -> bulkUpdate());
		updateButton.setEnabled(false);
		updateButton.setStyleName(ValoTheme.BUTTON_TINY);
	}

	// grid
	private void initGridComponent() {
		grid.setColumns("name", "address", "rating");
		grid.addColumn(hotel -> hotel.getCategory() == null ? "Not defined" : hotel.getCategory().getCategory(),
				new HtmlRenderer()).setCaption("Category");
		grid.addColumn(hotel -> "<a href='" + hotel.getUrl() + "' target='_blank'>URL</a>", new HtmlRenderer())
				.setCaption("Info link").setId("url");
		grid.addColumn("operatesFrom");
		grid.addColumn("description");
		grid.sort("name", SortDirection.DESCENDING);
		form.setVisible(false);
		grid.setSelectionMode(SelectionMode.MULTI);
		grid.asMultiSelect().addValueChangeListener(e -> {
			if (grid.getSelectedItems().size() > 1) {
				updateButton.setEnabled(true);
				deleteButton.setEnabled(true);
				form.setVisible(false);
				editButton.setEnabled(false);
			} else if (grid.getSelectedItems().size() == 1) {
				editButton.setEnabled(true);
				deleteButton.setEnabled(true);
				updateButton.setEnabled(false);
				form.setVisible(false);
			} else {
				updateButton.setEnabled(false);
				form.setVisible(false);
				editButton.setEnabled(false);
				deleteButton.setEnabled(true);
			}
			});
		grid.setSizeFull();
		grid.setHeight(31, Unit.EM);
		updateList();
	}

	// update hotel
	public void updateList() {
		List<Hotel> hotels = EntityService.getHotelService().findAll();
		grid.setItems(hotels);
	}

	// update hotel by name
	public void updateListByName() {
		List<Hotel> hotels = EntityService.getHotelService().findAllByName(filterName.getValue());
		grid.setItems(hotels);
	}

	// update hotel by address
	public void updateListByAddress() {
		List<Hotel> hotels = EntityService.getHotelService().findAllByAddress(filterAddress.getValue());
		grid.setItems(hotels);
	}

	@Override
	protected void delete() {
		Set<Hotel> selected = grid.getSelectedItems();
		selected.forEach(item -> EntityService.getHotelService().deleteHotel(item));
		form.closeForm();
		updateList();
	}

	@Override
	protected void edit() {
		Set<Hotel> items = grid.asMultiSelect().getSelectedItems();
		form.setHotel(items.iterator().next());
	}

	@Override
	protected void add() {
		Hotel h = new Hotel();
		form.setVisible(true);
		form.setHotel(h);
	}

	private void bulkUpdate() {
		popup.setDescription("Click to edit");
		popup.setHideOnMouseOut(false);
		addComponent(popup);
		popup.setPopupVisible(true);
		popup.setSizeFull();
	}
	public void hidePopup() {
		popup.setPopupVisible(false);
		popupContent.setHotels(new HashSet<Hotel>());
	}
}

package com.jpsolution.vaadin.app;

import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;

import com.jpsolution.vaadin.app.views.CategoryView;
import com.jpsolution.vaadin.app.views.HotelView;
import com.jpsolution.vaadin.entity.Category;
import com.jpsolution.vaadin.service.EntityService;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ContextLoaderListener;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.EnableVaadin;

import com.vaadin.ui.UI;

import java.util.List;

@SuppressWarnings("serial") //ignore the lack of serialVersionUID for serializable classes
@Theme("myTheme") //custom themes
@SpringUI //annotation to be put on UI-subclasses that are to be automatically detected and configured by Spring
public class MyUI extends UI{
    /**
     * Hotel search service
     */
    private static final String PAGE_TITLE = "Hotels service";
    private MenuBar menuBar;
    private Navigator navigator;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        // set name of the page
        getPage().setTitle(PAGE_TITLE);
        // init test data
        initData();
        // create layout
        VerticalLayout mainLayout = initMainView();
        // add menu
        mainLayout.addComponents(getMenuBar());
        // create panel for main layout
        Panel mainPanel = initMainPanel();
        mainLayout.addComponent(mainPanel);
        mainLayout.setExpandRatio(mainPanel, 100);
        super.setId("mainView");
        setContent(mainLayout);
        setSizeFull();
        // initialize navigator
        navigator = new Navigator(this, mainPanel);
        // register views
        navigator.addView(HotelView.VIEW_NAME, HotelView.class);
        navigator.addView(CategoryView.VIEW_NAME, CategoryView.class);
        // navigate to hotels
        navigator.navigateTo(HotelView.VIEW_NAME);
    }

    // data init
    private void initData() {
        EntityService.getCategoryService().fillTestData();
        List<Category> categories = EntityService.getCategoryService().findAll();
        EntityService.getHotelService().fillTestData(categories);
    }

    // verticalLayout init
    private VerticalLayout initMainView() {
        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setId("mainLayout");
        mainLayout.setSizeFull();
        // add style and make look a bit clean
        addStyleName(ValoTheme.LABEL_LIGHT);
        mainLayout.setMargin(false);
        return mainLayout;
    }

    //panel init
    private Panel initMainPanel() {
        Panel mainPanel = new Panel();
        mainPanel.setSizeFull();
        mainPanel.setId("mainPanel");
        return mainPanel;
    }

   // MenuBar
    private MenuBar getMenuBar() {
        if (menuBar != null) {
            return menuBar;
        }
        // create menu bar
        menuBar = new MenuBar();
        // create menu items
        menuBar.addItem("Hotel", VaadinIcons.BUILDING, command -> navigator.navigateTo(HotelView.VIEW_NAME));
        menuBar.addItem("",VaadinIcons.VAADIN_H, null).setEnabled(false);
        menuBar.addItem("Categories",VaadinIcons.LINES, command -> navigator.navigateTo(CategoryView.VIEW_NAME));
        menuBar.setStyleName(ValoTheme.LABEL_LIGHT);
        return menuBar;
    }

    @WebServlet(urlPatterns = "/*", name = "HotelUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {}

    @Configuration
    @EnableVaadin
    public static class MyConfiguration {
        @Bean
        public static EntityService getEntityService() {
            return new EntityService();
        };
    }

    @WebListener
    public static class MyContextLoaderListener extends ContextLoaderListener {}

}
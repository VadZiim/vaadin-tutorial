package com.jpsolution.vaadin.app.form;

import com.jpsolution.vaadin.app.converter.DataConverter;
import com.jpsolution.vaadin.app.views.HotelView;
import com.jpsolution.vaadin.entity.Hotel;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PopupForm extends HotelForm {

    private VerticalLayout layout = new VerticalLayout();
    private HorizontalLayout buttons = new HorizontalLayout();
    private Button updateBulk, closeBulk;
    private ComboBox<Field> comboBox = new ComboBox<>("Field to update");
    private TextField valueField = new TextField("Fild value");
    private Label label = new Label("Bulk Update");
    private List<Field> formFields;
    private Binder<Hotel> newBinder = new Binder<>(Hotel.class);

        public PopupForm(HotelView view) {
        super(view);
        setUpForms();
    }

    @Override
    public void setHotels(Set<Hotel> hotels) {
        this.hotels = hotels;
        manageFields(null);
    }

    @Override
    public void save() {
        try {
            for (Hotel hotel : hotels) {
                newBinder.writeBean(hotel);
                hotelService.saveHotel(hotel);
            }
        } catch (ValidationException e) {
            Notification.show("Hotel could not be saved");
            return;
        }
        hotelUI.updateList();
        hotelUI.hidePopup();
        hotel = null;
    }

    private void setUpForms() {
        removeAllComponents();
        layout.addComponents(label, comboBox, valueField, buttons);
        addComponent(layout);
        updateBulk = new Button("Save");
        updateBulk.addStyleName(ValoTheme.BUTTON_PRIMARY);
        updateBulk.addClickListener(e -> save());
        closeBulk = new Button("Close");
        closeBulk.addClickListener(e -> hotelUI.hidePopup());
        buttons.addComponents(updateBulk, closeBulk);

        List<Field> fields = Arrays.asList(HotelForm.class.getDeclaredFields());
        formFields = fields.stream().filter(field ->
                AbstractComponent.class.isAssignableFrom(field.getType()) && !field.getName().equals("view"))
                .collect(Collectors.toList());
        //setting items to combo box
        comboBox.setItems(formFields);
        comboBox.setItemCaptionGenerator(Field::getName);
        comboBox.setPlaceholder("Please, select field");
        comboBox.addSelectionListener(event -> manageFields(event.getValue()));
        manageFields(null);
    }

    private void manageFields(Field selected) {
        comboBox.setSelectedItem(null);
        for (Field field : formFields) {
            try {
                AbstractComponent component = (AbstractComponent) field.get(this);
                if (selected != null && selected.getName().equals(field.getName())) {
                    component.setVisible(true);
                    bindFieldByName(selected.getName());
                }
            } catch (IllegalAccessException e) {
                Logger.getLogger(PopupForm.class.getName())
                        .log(Level.SEVERE, "No access to the field");
            }
        }
    }

    private void bindFieldByName(String name) {
        newBinder = new Binder<>(Hotel.class);
        switch(name) {
            case "name": {
                newBinder.forField(this.name)
                        .asRequired("Name is not null")
                        .bind(Hotel:: getName, Hotel:: setName);
                break;
            }case "address": {
                newBinder.forField(address)
                        .asRequired("Address is not null")
                        .bind(Hotel:: getAddress, Hotel:: setAddress);
                break;
            }case "category": {
                newBinder.forField(category)
                        .asRequired("Category is not null")
                        .bind(Hotel:: getCategory, Hotel:: setCategory);
                break;
            }case "rating": {
                newBinder.forField(rating)
                        .asRequired("Rating is not null")
                        .withConverter(new StringToIntegerConverter(0, "Only digits!"))
                        .withValidator(v -> (v < 6 ), "Rating is not > 5")
                        .withValidator(v -> (v >= 0), "Rating is not < 0")
                        .bind(Hotel:: getRating, Hotel:: setRating);
                break;
            }case "url": {
                newBinder.forField(url)
                        .asRequired("Url is not null")
                        .bind(Hotel:: getUrl, Hotel:: setUrl);
                break;
            }case "operatesFrom": {
                newBinder.forField(operatesFrom)
                        .asRequired("OperatesFrom is not null")
                        .withConverter(new DataConverter())
                        .withValidator(v -> (v >= 0), "OperatesFrom is not > Today")
                        .bind(Hotel:: getOperatesFrom, Hotel:: setOperatesFrom);
                break;
            }case "description": {
                newBinder.forField(description)
                        .bind(Hotel:: getDescription, Hotel:: setDescription);
                break;
            }
        }
    }
}

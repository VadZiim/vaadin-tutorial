package com.jpsolution.vaadin.app.form;

import com.jpsolution.vaadin.app.views.CategoryView;
import com.jpsolution.vaadin.entity.Category;
import com.jpsolution.vaadin.service.CategoryService;
import com.jpsolution.vaadin.service.EntityService;
import com.vaadin.data.ValidationException;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@SpringUI
public class CategoryForm extends AbstractForm<Category> {

	private TextField name = new TextField("Name");

	private CategoryService categoryDAO;

	private Category category;

	private CategoryView categoryUI;

	public CategoryForm(CategoryView myUI) {
		super();
		this.categoryDAO = EntityService.getCategoryService();
		this.categoryUI = myUI;

		addComponents(name, buttons);

		bindFields();
	}

	public void setItem(Category category) {
		this.category = category;
		binder.readBean(this.category);
	}

	@Override
	protected void save() {
		// to make all validate marks appear
		binder.validate();
		if (binder.isValid()) {
			try {
				binder.writeBean(category);
				categoryDAO.saveCategory(category);
				Notification not = new Notification("Category saved sucessfully");
				not.show(UI.getCurrent().getPage());
				categoryUI.updateList();
				closeForm();
			} catch (ValidationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			Notification not = new Notification("Please check the form");
			not.show(UI.getCurrent().getPage());
		}
	}

	@Override
	protected void bindFields() {
		binder.forField(name).asRequired("Name is required").bind(Category::getCategory,
				Category::setCategory);
	}

}

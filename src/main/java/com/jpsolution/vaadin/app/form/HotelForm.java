package com.jpsolution.vaadin.app.form;

import com.jpsolution.vaadin.app.views.HotelView;
import com.jpsolution.vaadin.app.converter.DataConverter;
import com.jpsolution.vaadin.entity.Hotel;
import com.jpsolution.vaadin.entity.Category;
import com.jpsolution.vaadin.service.EntityService;
import com.jpsolution.vaadin.service.HotelService;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.ui.*;

import java.util.Set;

public class HotelForm extends AbstractForm<Hotel> {

	private static final long serialVersionUID = -2249768301112428822L;
	protected TextField name = new TextField("Name");
	protected TextField address = new TextField("Address");
	protected TextField rating = new TextField("Rating");
	protected NativeSelect<Category> category = new NativeSelect<>("Category");
	protected DateField operatesFrom = new DateField("Since");
	protected TextField url = new TextField("URL");
	protected TextArea description = new TextArea("Description");

	protected HotelService hotelService;
	protected Set<Hotel> hotels;// b
	protected Hotel hotel;
	protected HotelView hotelUI;

	public HotelForm(HotelView myUI) {
		super();
		this.hotelUI = myUI;
		this.hotelService = EntityService.getHotelService();

		addComponents(name, address, rating, category, operatesFrom, url, description, buttons);
		category.setItems(EntityService.getCategoryService().findAll());
		category.setItemCaptionGenerator(item -> item.getCategory());
		category.setWidth(11, Unit.EM);
		toolTipFields();
		bindFields();
	}

	public void bindFields(){
		binder.forField(rating)
			  .asRequired("Rating is not null")
			  .withConverter(new StringToIntegerConverter(0, "Only digits!"))
			  .withValidator(v -> (v < 6 ), "Rating is not > 5")
			  .withValidator(v -> (v >= 0), "Rating is not < 0")
			  .bind(Hotel:: getRating, Hotel:: setRating);

		binder.forField(operatesFrom)
			  .asRequired("OperatesFrom is not null")
			  .withConverter(new DataConverter())
			  .withValidator(v -> (v >= 0), "OperatesFrom is not > Today")
			  .bind(Hotel:: getOperatesFrom, Hotel:: setOperatesFrom);

		binder.forField(name)
			  .asRequired("Name is not null")
			  .bind(Hotel:: getName, Hotel:: setName);

		binder.forField(address)
		  	  .asRequired("Address is not null")
			  .bind(Hotel:: getAddress, Hotel:: setAddress);

		binder.forField(category)
		  	  .asRequired("Category is not null")
			  .bind(Hotel:: getCategory, Hotel:: setCategory);

		binder.forField(url)
		  	  .asRequired("Url is not null")
			  .bind(Hotel:: getUrl, Hotel:: setUrl);

		binder.forField(description)
			  .bind(Hotel:: getDescription, Hotel:: setDescription);
	}

	public void toolTipFields(){
		name.setDescription("Enter the name of the hotels");
		address.setDescription("Enter the address of the hotels");
		rating.setDescription("Enter the hotels rating");
		operatesFrom.setDescription("Enter from what date does the hotels operate");
		category.setDescription("Enter hotels category");
		url.setDescription("Enter the link to the hotels's website");
		description.setDescription("Enter your description of the hotels");
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
		binder.setBean(hotel);
		setVisible(true);
		name.selectAll();
		binder.validate();
	}

	public void setHotels(Set<Hotel> hotels) {//b>
		this.hotels = hotels;
		for (Hotel hotel : hotels) {
			binder.readBean(hotel);
		}
		setVisible(true);
		name.selectAll();
	}

	@Override
	protected void save() {
		binder.validate();
		if (binder.isValid()) {
			try {
				binder.writeBean(hotel);
				hotelService.saveHotel(hotel);
				hotelUI.updateList();
				closeForm();
			} catch (ValidationException e) {
				e.printStackTrace();
			}
		} else {
			Notification notification = new Notification(
					"There are some unvalid values, please check the input and try again.");
			notification.show(UI.getCurrent().getPage());
		}
	}
}
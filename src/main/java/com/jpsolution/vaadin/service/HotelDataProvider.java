package com.jpsolution.vaadin.service;

import com.jpsolution.vaadin.entity.Hotel;
import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;

import java.util.stream.Stream;

public class HotelDataProvider<Filter> extends AbstractBackEndDataProvider<Hotel, Filter> {

	private HotelService hotelService;

	public HotelDataProvider() {
		hotelService = EntityService.getHotelService();
	}

	@Override
	protected Stream<Hotel> fetchFromBackEnd(Query<Hotel, Filter> query) {

		return null;
	}

	@Override
	protected int sizeInBackEnd(Query<Hotel, Filter> query) {
		// TODO Auto-generated method stub
		return 0;
	}

}

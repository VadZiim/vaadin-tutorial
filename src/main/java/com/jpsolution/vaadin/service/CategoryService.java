package com.jpsolution.vaadin.service;

import com.jpsolution.vaadin.entity.Category;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class CategoryService {

    public CategoryService() {
    }

    @PersistenceContext(unitName = "demo_hotels", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    private static final Logger LOGGER = Logger.getLogger(CategoryService.class.getName());

    @Transactional
    public List<Category> findAll() {
        return entityManager.createQuery("select c from Category c", Category.class).getResultList();
    }

    public List<Category> findAll(String filter, int start, int maxResults) {
        return entityManager.createQuery("select c from Category c", Category.class).setFirstResult(start)
                .setMaxResults(maxResults).getResultList();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteCategory(Category category) {
        Category rem = entityManager.find(Category.class, category.getId());
        entityManager.remove(rem);
        entityManager.flush();
    }

    @Transactional
    public void saveCategory(Category entry) {
        if (entry == null) {
            LOGGER.log(Level.SEVERE, "Category is null.");
            return;
        }
        Category category = entityManager.merge(entry);
        entityManager.persist(category);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void fillTestData() {
        if (findAll().isEmpty()) {
            final String[] data = new String[]{"Hotel", "Hostel", "GuestHouse", "Appartments"};

            for (String category : data) {
                Category cat = new Category();
                cat.setCategory(category);
                saveCategory(cat);
            }
            entityManager.flush();
        }
    }
}

